-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 
-- サーバのバージョン： 10.1.37-MariaDB
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `techtest`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `corocororanking`
--

CREATE TABLE `corocororanking` (
  `Id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `corocororanking`
--

INSERT INTO `corocororanking` (`Id`, `Name`, `Score`) VALUES
(1, 'Player_A', 50000),
(2, 'Player_B', 40000),
(3, 'Player_C', 30000),
(4, 'Player_D', 20000),
(5, 'Player_E', 10000),
(6, 'Player_F', 25000),
(7, 'Test', 55000),
(8, 'yoshida', 27250),
(9, 'sample', 0),
(10, 'Yoshida', 79800),
(11, 'aaa', 0),
(12, 'Y.T', 77950);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `corocororanking`
--
ALTER TABLE `corocororanking`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `corocororanking`
--
ALTER TABLE `corocororanking`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
