﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class GetRankingManager : MonoBehaviour
{
    [Header("RankingData")]
    [SerializeField]
    Text rankingData = default;

    private List<PlayerData> _playerList;

    /// <summary>
    /// GetRankingボタンが押されたらランキングデータを取得
    /// </summary>
    public void OnClickGetRanking()
    {
        rankingData.text = "データ取得中...";
        GetRankingFromWebRequest();
    }

    /// <summary>
    /// ShowRankingボタンが押されたら取得してきたランキングデータを表示
    /// </summary>
    public void OnClickShowRanking()
    {
        string sStrOutput = "";
        int rank = 1;

        if (null == _playerList)
        {
            // ランキングデータが空の場合
            sStrOutput = "データがありません";
        }
        else
        {
            // Listの内容を表示
            foreach (PlayerData playerData in _playerList)
            {
                sStrOutput += $"{rank}位:{playerData.Name} スコア:{playerData.Score}\n";
                rank++;
            }
        }
        rankingData.text = sStrOutput;
    }

    /// <summary>
    /// ランキングのjsonデータを取得
    /// </summary>
    private void GetRankingFromWebRequest()
    {
        // jsonデータ取得のリクエストを行う
        StartCoroutine(
            DownloadJson(
                CallbackWebRequestSuccess,  // APIコールが成功した際に呼ばれる関数を指定
                CallbackWebRequestFailed    // APIコールが失敗した際に呼ばれる関数を指定
            )
        );
    }

    /// <summary>
    /// jsonデータ取得成功時の処理
    /// </summary>
    /// <param name="rensponse">Response.</param>
    private void CallbackWebRequestSuccess(string rensponse)
    {
        // jsonデータの内容をPlayerData型のListとしてデコード
        _playerList = PlayerDataModel.DeserializeFromJson(rensponse);

        // playerList・・・デコードされたプレイヤーリストが格納されている

        rankingData.text = "取得成功！";
    }

    /// <summary>
    /// jsonデータ取得失敗時の処理
    /// </summary>
    private void CallbackWebRequestFailed()
    {
        rankingData.text = "データ取得に失敗しました";
    }

    /// <summary>
    /// jsonデータのダウンロード
    /// </summary>
    /// <param name="cbkSuccess">Cbk success.</param>
    /// <param name="cbkFailed">Cbk failed.</param>
    /// <returns>The json.</returns>
    private IEnumerator DownloadJson(Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        // jsonデータの取得先URL
        UnityWebRequest www = UnityWebRequest.Get("http://localhost/corocoroapi/corocororanking/getRankings");
        // リモートサーバーとの通信を開始
        yield return www.SendWebRequest();

        if (www.error != null)
        {
            // レスポンスエラーの場合
            Debug.LogError(www.error);
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (www.isDone)
        {
            // リクエスト成功の場合
            Debug.Log($"成功:{www.downloadHandler.text}");
            if (null != cbkSuccess)
            {
                cbkSuccess(www.downloadHandler.text);
            }
        }
    }
}
