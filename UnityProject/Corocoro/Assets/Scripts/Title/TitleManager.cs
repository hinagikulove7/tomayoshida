﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleManager : MonoBehaviour
{
    [Header("RankingBoard")]
    [SerializeField]
    GameObject rankingBoard = default;
    [Header("RankingButton")]
    [SerializeField]
    Button rankingButton = default;
    [Header("CloseButton")]
    [SerializeField]
    Button closeButton = default;

    void Start()
    {
        rankingBoard.SetActive(false);
        rankingButton.onClick.AddListener(Ranking);
        closeButton.onClick.AddListener(CloseRanking);
    }

    /// <summary>
    /// ランキング表示
    /// </summary>
    public void Ranking()
    {
        rankingBoard.SetActive(true);
    }

    /// <summary>
    /// ランキング画面を閉じる
    /// </summary>
    public void CloseRanking()
    {
        rankingBoard.SetActive(false);
    }
}
