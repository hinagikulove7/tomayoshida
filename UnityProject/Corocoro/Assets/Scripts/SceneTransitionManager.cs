﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransitionManager : MonoBehaviour
{
    /// <summary>
    /// シーンの遷移を行う
    /// </summary>
    /// <param name="sceneName"></param>
    public void LoadTo(string sceneName)
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(sceneName);
        Debug.Log(sceneName);
    }
}
