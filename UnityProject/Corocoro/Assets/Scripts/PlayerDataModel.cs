﻿using System.Collections;
using System.Collections.Generic;
using MiniJSON;

public class PlayerDataModel
{
    /// <summary>
    /// PlayerData型のリストがjsonに入っていると仮定
    /// </summary>
    /// <param name="sStrJson">S string json.</param>
    /// <returns>The from json.</returns>
    public static List<PlayerData> DeserializeFromJson(string sStrJson)
    {
        var ret = new List<PlayerData>();

        // JSONデータは最初は配列から始まるのでDeserialize(デコード)した直後にListへキャスト
        IList jsonList = (IList)Json.Deserialize(sStrJson);

        // Listの内容はオブジェクトなので、辞書型の変数に一つ一つ代入しながら処理
        foreach (IDictionary jsonOne in jsonList)
        {
            // レコード解析開始
            var tmp = new PlayerData();

            // 該当するキーが jsonOne に存在するか調べ、存在するならば取得して変数に格納
            if (jsonOne.Contains("Name"))
            {
                tmp.Name = (string)jsonOne["Name"];
            }
            if (jsonOne.Contains("Score"))
            {
                tmp.Score = (long)jsonOne["Score"];
            }

            // レコード解析終了
            ret.Add(tmp);
        }
        return ret;
    }
}
