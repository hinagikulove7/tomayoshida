﻿public class PlayerData
{
    public string Name { get; set; }
    public long Score { get; set; }

    public PlayerData()
    {
        Name = "";
        Score = 0;
    }
}
