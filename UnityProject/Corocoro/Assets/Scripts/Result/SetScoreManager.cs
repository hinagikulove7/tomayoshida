﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class SetScoreManager : MonoBehaviour
{
    [Header("ToTitleButton")]
    [SerializeField]
    GameObject toTitleButton = default;
    [Header("DataText")]
    [SerializeField]
    Text dataText = default;
    [Header("NameText")]
    [SerializeField] 
    Text nameText = default;

    int scoreData;

    void Start()
    {
        toTitleButton.SetActive(false);
        scoreData = ScoreManager.score;
        dataText.text = "";
    }

    /// <summary>
    /// jsonデータ取得失敗時の処理
    /// </summary>
    private void CallbackWebRequestFailed()
    {
        dataText.text = "データ取得に失敗しました";
    }

    public void OnClickSetScore()
    {
        dataText.text = "データ送信中...";
        SetJsonFromWww();
    }

    private void SetJsonFromWww()
    {
        // jsonデータの送信先URL
        string sTgtURL = "http://localhost/corocoroapi/corocororanking/setRanking";
        string name = nameText.text;
        string score = scoreData.ToString();

        StartCoroutine(SetScore(sTgtURL, name, score, WebRequestSuccess, CallbackWebRequestFailed));
    }

    private IEnumerator SetScore(string url, string name, string score, Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("name", name);
        form.AddField("score", score);

        UnityWebRequest webRequest = UnityWebRequest.Post(url, form);

        webRequest.timeout = 5;

        yield return webRequest.SendWebRequest();

        if (webRequest.error != null)
        {
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (webRequest.isDone)
        {
            Debug.Log($"成功:{webRequest.downloadHandler.text}");
            toTitleButton.SetActive(true);
            if (null != cbkSuccess)
            {
                cbkSuccess(webRequest.downloadHandler.text);
            }
        }
    }

    private void WebRequestSuccess(string response)
    {
        dataText.text = response;
    }
}
