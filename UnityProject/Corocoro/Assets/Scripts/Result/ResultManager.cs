﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultManager : MonoBehaviour
{
    [Header("ResultText")]
    [SerializeField]
    Text resultText = default;

    [Header("効果音を鳴らすAudioSource")]
    [SerializeField]
    AudioSource resultAudio = default;
    [Header("リザルトSE")]
    [SerializeField]
    AudioClip resultSE = default;

    void Start()
    {
        int result = ScoreManager.score;
        resultAudio.PlayOneShot(resultSE, 0.4f);
        Debug.Log("今回の結果・・・" + result);
        resultText.text = "今回の結果・・・" + result.ToString("000000");
    }
}
