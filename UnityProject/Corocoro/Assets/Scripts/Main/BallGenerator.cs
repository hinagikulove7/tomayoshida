﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;  // Enumクラス利用

public class BallGenerator : MonoBehaviour
{
    [Header("BallObject")]
    [SerializeField]
    GameObject ballObj = default;
    [Header("TouchManager")]
    [SerializeField]
    TouchManager touchManager = default;

    // BallObjectの生成タイミングをカウントアップする変数
    int cnt = 0;
    int MAXCNT;

    // 生成したBallObjectの数を記録させる変数
    int generateCount = 0;

    void Start()
    {
        StartCoroutine(DropBall(21));
    }

    void FixedUpdate()
    {
        // 制限時間を過ぎたらBallObjectの生成をストップする
        if (GameManager.time > 0)
        {
            // FEVERタイム中はBallの生成を速くする
            switch (touchManager.isFever)
            {
                case true:
                    MAXCNT = 18;
                    break;
                case false:
                    MAXCNT = 30;
                    break;
            }

            cnt++;
            cnt %= MAXCNT;
            if (cnt == 0)
            {
                generateCount++;
                generateCount %= 12;

                GameObject gameObject = Instantiate(ballObj);
                // 親オブジェクト・・・BallGenerator
                gameObject.transform.parent = this.transform;
                gameObject.transform.localPosition = Vector3.zero;
                // -60°～60°の範囲でボールが落ちる方向を変える
                gameObject.GetComponent<Rigidbody>().AddForce(Quaternion.Euler(0, 0, UnityEngine.Random.Range(-60.0f, 60.0f)) * Vector3.down * 10f, ForceMode.Impulse);

                if (generateCount == 0)
                {
                    // 12回毎にbombを生成
                    gameObject.GetComponent<BallObject>().color = Enum.GetValues(typeof(GameResources.BallColor)).Cast<GameResources.BallColor>().ToList()[4];
                }
                else
                {
                    // 生成するBallObjectのColorをランダムに設定
                    gameObject.GetComponent<BallObject>().color = Enum.GetValues(typeof(GameResources.BallColor)).Cast<GameResources.BallColor>().ToList()[UnityEngine.Random.Range(0, 4)];
                }
            }
        }
    }

    /// <summary>
    /// ゲーム開始時に指定した数のBallを降らせる
    /// </summary>
    /// <param name="count">Ballの数</param>
    IEnumerator DropBall(int count)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject gameObject = Instantiate(ballObj);
            // 親オブジェクト・・・BallGenerator
            gameObject.transform.parent = this.transform;
            gameObject.transform.localPosition = Vector3.zero;
            // -60°～60°の範囲でボールが落ちる方向を変える
            gameObject.GetComponent<Rigidbody>().AddForce(Quaternion.Euler(0, 0, UnityEngine.Random.Range(-60.0f, 60.0f)) * Vector3.down * 30f, ForceMode.Impulse);
            // 生成するBallObjectのColorをランダムに設定
            gameObject.GetComponent<BallObject>().color = Enum.GetValues(typeof(GameResources.BallColor)).Cast<GameResources.BallColor>().ToList()[UnityEngine.Random.Range(0, 4)];
            yield return new WaitForSeconds(0.1f);
        }
    }
}
