﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteEffectObject : MonoBehaviour
{
    [Header("DeleteEffectAnimator")]
    [SerializeField]
    Animator animator = default;

    void Update()
    {
        // AnimatorControllerの状態が"Empty"になった時自動的に消える処理
        // GetCurrentAnimatorStateInfo(0)・・・Base Layerを指定
        AnimatorStateInfo a = animator.GetCurrentAnimatorStateInfo(0);
        if (a.IsName("Empty"))
        {
            Destroy(this.gameObject);
        }
    }
}
