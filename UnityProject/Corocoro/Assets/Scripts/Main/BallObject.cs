﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallObject : MonoBehaviour
{
    [Header("参照するMaterial(BallObject)")] 
    [SerializeField]
    public new Renderer renderer;
    [Header("タッチしたBallObjectが選択状態かどうか")]
    [SerializeField]
    public bool isTouch = false;
    [Header("BallObjectの種類を設定")]
    [SerializeField]
    public GameResources.BallColor color;

    void Start()
    {
        ChangeColor();
    }

    void Update()
    {
        if (isTouch)
        {
            GetComponent<BallObject>().renderer.material.SetColor("_EmissionColor", new Color(0.5f, 0.5f, 0f));
        }
        else
        {
            GetComponent<BallObject>().renderer.material.SetColor("_EmissionColor", new Color(0f, 0f, 0f));
        }
    }

    // 変数colorによってAlbedo(BallObject)の色を変更する関数
    public void ChangeColor()
    {
        switch (color)
        {
            case GameResources.BallColor.red:
                GetComponent<BallObject>().renderer.material.SetColor("_Color", Color.red);
                break;
            case GameResources.BallColor.blue:
                GetComponent<BallObject>().renderer.material.SetColor("_Color", Color.blue);
                break;
            case GameResources.BallColor.green:
                GetComponent<BallObject>().renderer.material.SetColor("_Color", Color.green);
                break;
            case GameResources.BallColor.purple:
                GetComponent<BallObject>().renderer.material.SetColor("_Color", new Color(1, 0, 1));
                break;
            case GameResources.BallColor.bomb:
                GetComponent<BallObject>().renderer.material.SetColor("_Color", new Color(0, 0, 0));
                break;
        }
    }

    /// <summary>
    /// 周囲のBallObjectを消去
    /// </summary>
    public void Explosion(GameObject deleteObj)
    {
        // bombの半径5mの範囲にあるBallObjectを全て消去
        RaycastHit[] hits = Physics.SphereCastAll(transform.position, 5.0f, Vector3.forward);
        GameObject delObj;

        foreach (RaycastHit hit in hits)
        {
            if (hit.collider.CompareTag("Ball"))
            {
                delObj = Instantiate(deleteObj);
                delObj.transform.position = hit.collider.gameObject.transform.position;
                Destroy(hit.collider.gameObject);
            }
        }
    }

    /// <summary>
    /// bombによって消したBallObjectの数をカウント
    /// </summary>
    /// <returns>消したBallObjectの数</returns>
    public int DestroyLength()
    {
        RaycastHit[] hits = Physics.SphereCastAll(transform.position, 5.0f, Vector3.forward);
        int length = hits.Length;
        return length;
    }
}
