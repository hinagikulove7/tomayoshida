﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [Header("ScoreText")]
    [SerializeField]
    Text scoreText = default;

    public static int score;

    /// <summary>
    /// scoreの増加
    /// </summary>
    /// <param name="point">scoreに加算する値</param>
    public void AddScore(int point)
    {
        score += point;
        scoreText.text = "" + score;
        Debug.Log("得点：" + point);
    }

    /// <summary>
    /// bombをクリックした場合のscore増加
    /// </summary>
    /// <param name="exPoint">scoreに加算する値</param>
    public void AddExplosionScore(int exPoint)
    {
        score += exPoint;
        scoreText.text = "" + score;
        Debug.Log("bombによる得点：" + exPoint);
    }
}
