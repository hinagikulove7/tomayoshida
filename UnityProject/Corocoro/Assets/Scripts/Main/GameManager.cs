﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    // 制限時間
    [Header("TimerText")]
    [SerializeField]
    Text timerText = default;

    public static float time;

    // ゲームスタート前のカウントダウン
    [Header("CountPanel")]
    [SerializeField]
    GameObject countPanel = default;
    [Header("CountText")]
    [SerializeField]
    Text countText = default;

    float countdown = 4f;
    int count;

    // 一時停止
    [Header("PausePanel")]
    [SerializeField]
    GameObject pausePanel = default;
    [Header("PauseButton")]
    [SerializeField]
    Button pauseButton = default;

    // ゲーム再開
    [Header("ResumeButton")]
    [SerializeField]
    Button resumeButton = default;

    // ゲーム終了
    [Header("FinishPanel")]
    [SerializeField]
    GameObject finishPanel = default;
    [Header("FinishText")]
    [SerializeField]
    Text finishText = default;
    [Header("ブザーを鳴らすAudioSource")]
    [SerializeField]
    AudioSource finishAudio = default;
    [Header("ブザーSE")]
    [SerializeField]
    AudioClip finishSE = default;

    // 経過時間をカウント
    float stepTime;

    bool isFinish = false;

    [Header("SceneManager")]
    [SerializeField]
    SceneTransitionManager sceneTransitionManager  = default;

    void Start()
    {
        time = 30.0f;
        ScoreManager.score = 0;
        stepTime = 0f;

        countPanel.SetActive(true);
        pausePanel.SetActive(false);
        pauseButton.onClick.AddListener(Pause);
        resumeButton.onClick.AddListener(Resume);
        finishPanel.SetActive(false);
    }

    void Update()
    {
        if (!isFinish)
        {
            if (countdown >= 1)
            {
                countdown -= Time.deltaTime;
                count = (int)countdown;
                countText.text = count.ToString();
            }
            else if (countdown < 1 && countdown >= 0)
            {
                countdown -= Time.deltaTime;
                countText.text = "START!!!";
            }
            else
            {
                countPanel.SetActive(false);
                countText.text = "";

                time -= Time.deltaTime;
                timerText.GetComponent<Text>().text = time.ToString("F1");
                if (time < 0)
                {
                    time = 0;
                    isFinish = true;
                }
            }
        }
        else
        {
            Finish();
        }
    }

    /// <summary>
    /// 一時停止
    /// </summary>
    public void Pause()
    {
        Time.timeScale = 0;
        Debug.Log("一時停止");
        pausePanel.SetActive(true);
    }

    /// <summary>
    /// ゲーム再開
    /// </summary>
    public void Resume()
    {
        Time.timeScale = 1;
        Debug.Log("ゲーム再開");
        pausePanel.SetActive(false);
    }

    /// <summary>
    /// ゲーム終了
    /// </summary>
    public void Finish()
    {
        if (stepTime == 0)
        {
            finishAudio.PlayOneShot(finishSE, 0.4f);
        }
        stepTime += Time.deltaTime;
        finishPanel.SetActive(true);
        finishText.text = "FINISH!!!";
        if (stepTime >= 1.5f)
        {
            Debug.Log(ScoreManager.score);
            sceneTransitionManager.LoadTo("Result");
        }
    }
}
