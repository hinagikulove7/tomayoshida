﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchManager : MonoBehaviour
{
    [Header("なぞったBallObjectを格納するList")]
    [SerializeField]
    List<GameObject> touchBallList;
    [Header("DeleteEffectObject")]
    [SerializeField]
    GameObject deleteEffectObj = default;
    [Header("ScoreManager")]
    [SerializeField]
    ScoreManager scoreManager = default;

    [Header("効果音を再生するAudioSource")]
    [SerializeField]
    AudioSource soundEffectAudio = default;
    [Header("Ballをタッチした時のSE")]
    [SerializeField]
    AudioClip touchSE = default;
    [Header("Ballを消した時のSE")]
    [SerializeField]
    AudioClip deleteSE = default;

    [Header("BGMを再生するAudioSource")]
    [SerializeField]
    public AudioSource musicAudio = default;

    [Header("FeverGauge")]
    [SerializeField]
    Slider feverGauge = default;
    [Header("FeverText")]
    [SerializeField]
    Text feverText = default;

    [Header("BonusText")]
    [SerializeField]
    Text bonusText = default;

    int mul;                            // scoreの倍率
    float feverCount;                   // FEVERゲージの値
    float MINFeverCount = 0f;           // FeverGaugeの最小値
    float MAXFeverCount = 15f;          // FeverGaugeの最大値
    public bool isFever;                // FEVERタイム中かどうか
    int timeBonus;                      // 制限時間を増やす

    void Start()
    {
        // 初期化
        mul = 1;
        feverCount = 0;
        isFever = false;
        feverGauge.value = MINFeverCount;
        feverGauge.maxValue = MAXFeverCount;
        bonusText.text = "";
        timeBonus = 0;
    }

    void Update()
    {
        // FeverGaugeを減らす関数を呼ぶ
        UpdateFever();

        // マウスカーソルの位置を取得
        Vector3 mousePos = Input.mousePosition;
        if (Input.GetMouseButtonDown(0))
        {
            // Listの初期化
            touchBallList = new List<GameObject>();
            // マウスの位置から画面奥方向へRayを放つ
            Ray ray = Camera.main.ScreenPointToRay(mousePos);
            // Rayと衝突したBallObjectを全て検知してListに格納
            RaycastHit[] h = Physics.RaycastAll(ray, 100.0f);
            // タッチしたBallObjectが0個でないとき
            if (h.Length > 0)
            {
                // タッチしたBallObjectが選択状態でないとき
                if (h[0].collider.CompareTag("Ball") && !h[0].collider.GetComponent<BallObject>().isTouch)
                {
                    if (h[0].collider.GetComponent<BallObject>().color == GameResources.BallColor.bomb)
                    {
                        // 爆発
                        h[0].collider.GetComponent<BallObject>().Explosion(deleteEffectObj);
                        // 効果音を鳴らす
                        soundEffectAudio.clip = deleteSE;
                        soundEffectAudio.Play();

                        int length = h[0].collider.GetComponent<BallObject>().DestroyLength();
                        Debug.Log("bombで破壊したBallの数：" + length);
                        
                        int cnt = Mathf.RoundToInt(length / 3);
                        Debug.Log(cnt);

                        AddFeverGauge(cnt);

                        int explosionScore = (100 * cnt * (cnt + 1)) - 500;
                        scoreManager.AddExplosionScore(explosionScore / 2 * mul);
                    }
                    else
                    {
                        h[0].collider.GetComponent<BallObject>().isTouch = true;
                        // BallObjectをListへ追加
                        touchBallList.Add(h[0].collider.gameObject);
                        soundEffectAudio.clip = touchSE;
                        soundEffectAudio.Play();
                    }
                }
            }
        }
        if (Input.GetMouseButton(0))
        {
            // 選択状態のBallObjectが0個でないとき
            if (touchBallList.Count != 0)
            {
                // マウスの位置から画面奥方向へRayを放つ
                Ray ray = Camera.main.ScreenPointToRay(mousePos);
                // Rayと衝突したBallObjectを全て検知してListに格納
                RaycastHit[] h = Physics.RaycastAll(ray, 100.0f);
                if (h.Length > 0)
                {
                    // 「BallObject」かつ「選択したことのあるBallObjectでない」かつ「最初にタッチしたBallObjectの色と一致している」場合
                    if (h[0].collider.CompareTag("Ball") && !h[0].collider.GetComponent<BallObject>().isTouch && touchBallList[0].GetComponent<BallObject>().color == h[0].collider.GetComponent<BallObject>().color)
                    {
                        h[0].collider.GetComponent<BallObject>().isTouch = true;
                        // BallObjectをListへ追加
                        touchBallList.Add(h[0].collider.gameObject);
                        soundEffectAudio.clip = touchSE;
                        soundEffectAudio.Play();
                    }
                    // 「BallObject」かつ「最初にタッチしたBallObjectの色と一致していない」場合はReleaseObjectを実行
                    else if (h[0].collider.CompareTag("Ball") && touchBallList[0].GetComponent<BallObject>().color != h[0].collider.GetComponent<BallObject>().color)
                    {
                        ReleaseObject();
                    }
                }
                else
                {
                    ReleaseObject();
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            ReleaseObject();
        }
    }

    /// <summary>
    /// ボタンを離したらBallObjectを消去
    /// </summary>
    public void ReleaseObject()
    {
        int count = touchBallList.Count;
        const int MINCNT = 3;
        // 離したらマテリアルの色を戻す
        foreach (GameObject obj in touchBallList)
        {
            // 選択状態を解除
            obj.GetComponent<BallObject>().isTouch = false;
            // 3個以上ならBallObjectを消去
            if (count >= MINCNT)
            {
                GameObject delObj = Instantiate(deleteEffectObj);
                delObj.transform.position = obj.transform.position;
                Destroy(obj);
                // 効果音を鳴らす
                soundEffectAudio.clip = deleteSE;
                soundEffectAudio.Play();
            }
        }
        // Listの要素を全消去
        touchBallList.Clear();
        if (count >= MINCNT)
        {
            AddFeverGauge(count);

            Debug.Log("消したBallの数：" + count);
            int score = 50 * count * (count + 1) - 300;
            scoreManager.AddScore(score * mul);

            // BallObjectを4個以上消したら制限時間を増やす
            timeBonus = (count - MINCNT) * 2 - 1;
            if (timeBonus > 0)
            {
                GameManager.time += timeBonus;
                Debug.Log("ボーナス：" + timeBonus + "秒追加");
                StartCoroutine(SetBonusText());
            }
        }
    }

    /// <summary>
    /// FeverGaugeの増加
    /// </summary>
    /// <param name="num">Gaugeの増加量</param>
    public void AddFeverGauge(int num)
    {
        if (!isFever)
        {
            // 消したBallの数に応じてFeverGaugeを増やす
            feverCount += num;
            Debug.Log("feverCount：" + feverCount);
            feverGauge.value = feverCount;
            if (feverCount >= MAXFeverCount)
            {
                // 最大値を超えたらFEVERタイムへ
                feverCount = MAXFeverCount;
                feverGauge.value = feverCount;
                isFever = true;
                Debug.Log("feverCount：" + feverCount);

                timeBonus = 5;
                GameManager.time += timeBonus;
                Debug.Log("FEVERタイム! " + timeBonus + "秒追加");
                StartCoroutine(SetBonusText());

                // scoreを3倍にする
                mul = 3;
            }
        }
    }

    /// <summary>
    /// FeverGaugeを徐々に減らす
    /// </summary>
    public void UpdateFever()
    {
        if (GameManager.time > 0)
        {
            if (Time.timeScale != 0)
            {
                // FEVERタイム以外のとき
                if (!isFever)
                {
                    feverText.text = "";
                    feverCount -= 1f / 100f;
                    feverGauge.value = feverCount;
                    if (feverCount < MINFeverCount)
                    {
                        feverCount = MINFeverCount;
                        feverGauge.value = MINFeverCount;
                    }
                }
                // FEVERタイム中は素早く減らす
                else
                {
                    // BGMのピッチをあげる
                    musicAudio.pitch = 1.5f;
                    feverText.text = "FEVER!!!";
                    feverCount -= MAXFeverCount / 500f;
                    feverGauge.value = feverCount;
                    if (feverCount < MINFeverCount)
                    {
                        feverCount = MINFeverCount;
                        feverGauge.value = MINFeverCount;
                        isFever = false;
                        // BGMのピッチを戻す
                        musicAudio.pitch = 1;
                    }
                }
            }
        }
        else
        {
            feverText.text = "";
            feverCount = MINFeverCount;
            feverGauge.value = MINFeverCount;
            isFever = false;
            // BGMを停止する
            musicAudio.Stop();
        }
    }

    /// <summary>
    /// BonusTextの表示
    /// </summary>
    /// <returns></returns>
    IEnumerator SetBonusText()
    {
        bonusText.text = "Bonus +" + timeBonus + "Sec";
        yield return new WaitForSeconds(1.0f);
        bonusText.text = "";
    }
}