<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Corocororanking $corocororanking
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Corocororanking'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="corocororanking form large-9 medium-8 columns content">
    <?= $this->Form->create($corocororanking) ?>
    <fieldset>
        <legend><?= __('Add Corocororanking') ?></legend>
        <?php
            echo $this->Form->control('Name');
            echo $this->Form->control('Score');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
