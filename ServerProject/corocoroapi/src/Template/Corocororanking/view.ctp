<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Corocororanking $corocororanking
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Corocororanking'), ['action' => 'edit', $corocororanking->Id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Corocororanking'), ['action' => 'delete', $corocororanking->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $corocororanking->Id)]) ?> </li>
        <li><?= $this->Html->link(__('List Corocororanking'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Corocororanking'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="corocororanking view large-9 medium-8 columns content">
    <h3><?= h($corocororanking->Id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($corocororanking->Name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($corocororanking->Id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Score') ?></th>
            <td><?= $this->Number->format($corocororanking->Score) ?></td>
        </tr>
    </table>
</div>
