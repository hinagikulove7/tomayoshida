<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Corocororanking Controller
 *
 * @property \App\Model\Table\CorocororankingTable $Corocororanking
 *
 * @method \App\Model\Entity\Corocororanking[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CorocororankingController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $corocororanking = $this->paginate($this->Corocororanking);

        $this->set(compact('corocororanking'));
    }

    /**
     * View method
     *
     * @param string|null $id Corocororanking id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $corocororanking = $this->Corocororanking->get($id, [
            'contain' => []
        ]);

        $this->set('corocororanking', $corocororanking);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $corocororanking = $this->Corocororanking->newEntity();
        if ($this->request->is('post')) {
            $corocororanking = $this->Corocororanking->patchEntity($corocororanking, $this->request->getData());
            if ($this->Corocororanking->save($corocororanking)) {
                $this->Flash->success(__('The corocororanking has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The corocororanking could not be saved. Please, try again.'));
        }
        $this->set(compact('corocororanking'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Corocororanking id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $corocororanking = $this->Corocororanking->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $corocororanking = $this->Corocororanking->patchEntity($corocororanking, $this->request->getData());
            if ($this->Corocororanking->save($corocororanking)) {
                $this->Flash->success(__('The corocororanking has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The corocororanking could not be saved. Please, try again.'));
        }
        $this->set(compact('corocororanking'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Corocororanking id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $corocororanking = $this->Corocororanking->get($id);
        if ($this->Corocororanking->delete($corocororanking)) {
            $this->Flash->success(__('The corocororanking has been deleted.'));
        } else {
            $this->Flash->error(__('The corocororanking could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /*
    // ランキング表示(スコア降順)
    public function ranking()
    {
        // [corocororanking]テーブルからクエリを取得
        $query = $this->Corocororanking->find('all');

        // 現在のクエリの状態をデバッグ表示する
        // debug($query);

        // カラム['Score']をキーにして降順ソート
        $query->order(['Score' => 'DESC']);
        $query->limit(5);

        // クエリを実行してarrayにデータを格納
        $array = $query->toArray();

        // 全レコードのScoreを取得
        for ($i = 1; $i <= count($array); $i++) {
            $tmpRecord = $array[$i - 1];
            $this->set('ranking' . $i, $tmpRecord['Score']);
            $this->set('name' . $i, $tmpRecord['Name']);
        }
    }
    */

    // ランキングデータリストの取得
    public function getRankings()
    {
        $this->autoRender = false;

        $id = $this->request->getData('id');

        // [Corocororanking]テーブルからクエリを取得
        $query = $this->Corocororanking->find('all');

        // カラム['Score']をキーにして降順ソート
        $query->order(['score' => 'DESC']);
        $query->limit(5);

        // クエリを元にjson形式へ変換
        $json_array = json_encode($query);

        echo $json_array;
    }

    // ランキングデータのセット
    public function setRanking()
    {
        $this->autoRender = false;

        $postName   = $this->request->getData('name');
        $postScore  = $this->request->getData('score');

        $record = array('Name' => $postName, 'Score' => $postScore);

        $Corocororanking = $this->Corocororanking->newEntity();
        $Corocororanking = $this->Corocororanking->patchEntity($Corocororanking, $record);

        if ($this->Corocororanking->save($Corocororanking)) {
            echo "OK";
        } else {
            echo "NG";
        }
    }
}
