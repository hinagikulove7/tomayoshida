<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CorocororankingTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CorocororankingTable Test Case
 */
class CorocororankingTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CorocororankingTable
     */
    public $Corocororanking;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.corocororanking'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Corocororanking') ? [] : ['className' => CorocororankingTable::class];
        $this->Corocororanking = TableRegistry::getTableLocator()->get('Corocororanking', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Corocororanking);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
